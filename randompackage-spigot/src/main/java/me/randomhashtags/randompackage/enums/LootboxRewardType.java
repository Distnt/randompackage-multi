package me.randomhashtags.randompackage.enums;

public enum LootboxRewardType { BONUS, JACKPOT, REGULAR; }
