package me.randomhashtags.randompackage.enums;

public enum OutpostStatus {UNCONTESTED, CONTESTED, CONTROLLING, UNDER_ATTACK, SECURING}
